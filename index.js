// console.log("Hello World!");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function userInfo() {
        let userFullName = prompt("Enter your full name: ");
        let userAge = prompt("Enter your age: ");
        let userLoc = prompt("Enter your location: ");
        console.log("Hello, " + userFullName);
        console.log("You are " + userAge + " years old.");
        console.log("You live in " + userLoc);
    }

    userInfo(); 

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function favMusicArtist (){
        console.log("1. Linkin Park");
        console.log("2. Coldplay");
        console.log("3. Led Zeppelin");
        console.log("4. Nirvana");
        console.log("5. Red Hot Chili Peppers");
    }
    favMusicArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function favMovies (){
        console.log("1. The Matrix");
        console.log("Rotten Tomatoes Rating: 85%");
        console.log("2. Fight Club");
        console.log("Rotten Tomatoes Rating: 96%");
        console.log("3. Parasite");
        console.log("Rotten Tomatoes Rating: 90%");
        console.log("4. Wall-E");
        console.log("Rotten Tomatoes Rating: 90%");
        console.log("5. Forrest Gump");
        console.log("Rotten Tomatoes Rating: 95%");
    }

    favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


 
let printUserFriends = function () {
	// alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printUserFriends();
